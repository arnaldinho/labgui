# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 13:18:32 2012

Copyright (C) 10th april 2015  Benjamin Schmidt
License: see LICENSE.txt file

TODO:
    - add toolbar for plot navigation
    - more descriptive output in print 
    - disable appropriate inputs when acquisition starts
    - end thread more gracefully    
    - move instrument objects to main thread / shared?
    - print the right axes as well as the left
"""

from __future__ import division
import time

#from PyQt4.QtSvg import *
import PyQt4.QtGui as QtGui
from PyQt4.QtCore import QString, SIGNAL, Qt, QRect

import QtTools

import numpy as np, matplotlib.pyplot as plt


from mplZoomWidget import MatplotlibZoomWidget
import ui_plotdisplaywindow
from matplotlib import dates
from matplotlib import ticker

import datetime

import readconfigfile


# Not sure what this is for - it's copy-paste from some example.
try:
    from PyQt4.QtCore import QString
except ImportError:
    QString = str
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class PlotDisplayWindow(QtGui.QMainWindow, ui_plotdisplaywindow.Ui_PlotDisplayWindow):
    def __init__(self, parent=None, data_array=np.array([]),name="Main Window",default_channels=10):
        # run the initializer of the class inherited from
        super(PlotDisplayWindow, self).__init__()
                     
        # this is where most of the GUI is made
        self.setupUi(self)
        self.customizeUi(default_channels)

        autoHide =  QtTools.QAutoHideDockWidgets(Qt.RightDockWidgetArea, self) 
        
        # axes and figure initialization - short names for convenience   
        self.fig = self.mplwidget.figure
        self.setWindowTitle(name)
        self.ax = self.mplwidget.axes
        self.axR = self.mplwidget.axesR

        self.ax.xaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
        self.ax.yaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
        self.axR.yaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
        
        self.fig.canvas.draw()              
        
        # this is for a feature that doesn't exist yet
        self.history_length = 0

        self.num_channels = 0
        self.left_lines = [] 
        self.right_lines = [] 
        
        #Fills self.lineEdit_Name = [], self.comboBox_Type = [], self.comboBox_Instr = []. self.comboBox_Param = []
        #Whenever connect(obbject,SIGNAL(),function) is used it will call the function whenever the object is manipulated or something emits the same SIGNAL()
        for i in range (default_channels):   
            self.add_channel_controls()
        
        # objects to hold line data. Plot empty lists just to make handles
        # for line objects of the correct color   


        # create the empty data array and use channel 0 as X by default
        self.data_array = data_array#np.array([])
        self.chan_X = 0
        
    def customizeUi(self, default_channels):       
        #Contains the name of the value measured that will be used for axis labeling
        self.lineEdit_Name = []
        #Tick which measured quantity will be diplayed on X axis
        self.radioButton_X = [] 
        #Tick which measured quantity will be diplayed on Y left axis
        self.checkBox_Y = [] 
        #Tick which measured quantity will be diplayed on Y right axis
        self.checkBox_YR = []        
        
        self.checkBox_invert = []
        
        # create a layout within the blank "plot_holder" widget and put the 
        # custom matplotlib zoom widget inside it. This way it expands to fill
        # the space, and we don't need to customize the ui_recordsweep.py file
        self.gridLayout_2 = QtGui.QGridLayout(self.plot_holder)
        self.gridLayout_2.setMargin(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        
        self.mplwidget = MatplotlibZoomWidget(self.plot_holder)
        self.mplwidget.setObjectName(_fromUtf8("mplwidget")) 
        self.gridLayout_2.addWidget(self.mplwidget, 0, 0, 1, 1)
  

    
    def add_channel_controls (self):
        
        #index of boxes to create
        i = self.num_channels
        
        self.num_channels = self.num_channels + 1    

        pos_LE = lambda x: 20 * (x + 1)
        
        self.lineEdit_Name.append(QtGui.QLineEdit(self.groupBox_Name))
        self.lineEdit_Name[i].setGeometry(QRect(10, pos_LE(i), 81, 16))
        self.lineEdit_Name[i].setText(QtGui.QApplication.translate("RecordSweepWindow", "", None, QtGui.QApplication.UnicodeUTF8))
        self.lineEdit_Name[i].setObjectName(_fromUtf8("lineEdit_Name"))
        self.connect(self.lineEdit_Name[i],SIGNAL("textEdited(QString)"),self.lineEditHandler)
        self.lineEdit_Name[i].show()


        self.radioButton_X.append(QtGui.QRadioButton(self.groupBox_X))
        self.radioButton_X[i].setGeometry(QRect(7, 20*(i+1), 16, 16))
        self.radioButton_X[i].setText(_fromUtf8(""))
        self.radioButton_X[i].setObjectName(_fromUtf8("radioButton_" + str(i)))
        self.connect(self.radioButton_X[i], SIGNAL("toggled(bool)"), self.XRadioButtonHandler)  
        self.radioButton_X[i].show()        
                
        self.checkBox_Y.append(QtGui.QCheckBox(self.groupBox_Y))
        self.checkBox_Y[i].setGeometry(QRect(5, 20 * (i+1), 16, 16))
        self.checkBox_Y[i].setText(_fromUtf8(""))
        self.checkBox_Y[i].setObjectName(_fromUtf8("checkBox_" +str(i)))  
        self.connect(self.checkBox_Y[i], SIGNAL("stateChanged(int)"), self.YCheckBoxHandler)     
        self.checkBox_Y[i].show()        
        
        self.checkBox_YR.append(QtGui.QCheckBox(self.groupBox_YR))
        self.checkBox_YR[i].setGeometry(QRect(5, 20 * (i+1), 16, 16))
        self.checkBox_YR[i].setText(_fromUtf8(""))
        self.checkBox_YR[i].setObjectName(_fromUtf8("checkBox_" +str(i)))  
        self.connect(self.checkBox_YR[i], SIGNAL("stateChanged(int)"), self.YCheckBoxHandler)    
        self.checkBox_YR[i].show()
        
        self.checkBox_invert.append(QtGui.QCheckBox(self.groupBox_invert))
        self.checkBox_invert[i].setGeometry(QRect(5, 20 * (i+1), 16, 16))
        self.checkBox_invert[i].setText(_fromUtf8(""))
        self.checkBox_invert[i].setObjectName(_fromUtf8("checkBox_" +str(i)))  
        self.connect(self.checkBox_invert[i], SIGNAL("stateChanged(int)"), self.YCheckBoxHandler)    
        self.checkBox_invert[i].show()

        #create line objects and append them to self.ax[R].lines autoatically
        line1, = self.ax.plot([], [])     
        line2, = self.axR.plot([], [])
        
        
    def XRadioButtonHandler(self):
        print "X clicked"           
        for num, box in enumerate(self.radioButton_X):
            if box.isChecked():
                self.chan_X = num
                self.ax.set_xlabel(self.lineEdit_Name[num].text())
 
        self.update_plot() 
        
    def YCheckBoxHandler(self):  
        """Update which data is used for the Y axis (both left and right)"""
        label = ""
        print "Y clicked"        
        for num, box in enumerate(self.checkBox_Y):
            if box.isChecked():
                name = str(self.lineEdit_Name[num].text())
                #unit = self.UNITS[str(self.comboBox_Param[num].currentText())]
                label = label + name #+ " (" + unit + ")" + ", "
        self.ax.set_ylabel(label.rstrip(', '))

        label = ""
        for num, box in enumerate(self.checkBox_YR):
            if box.isChecked():
                name = str(self.lineEdit_Name[num].text())
                #unit = self.UNITS[str(self.comboBox_Param[num].currentText())]
                label = label + name #+ " (" + unit + ")" + ", "
        self.axR.set_ylabel(label.rstrip(', '))
        self.update_plot()             

    def set_axis_ticks(self,ticks):
        if not len(ticks)==3:
            print "some ticks are missing, you should have ticks for X, YL and YR axes"
        else:
            for t in ticks[1]:
                self.checkBox_Y[t].setCheckState(True)
#                print "Y",str(self.lineEdit_Name[t].text())
            for t in ticks[2]:
                self.checkBox_YR[t].setCheckState(True)
#                print "YR",str(self.lineEdit_Name[t].text())
                
            self.radioButton_X[ticks[0]].setChecked(True)
#            print "X",str(self.lineEdit_Name[ticks[0]].text())
            
    
    def get_X_axis_label(self):
        """Update which data is used for the X axis"""
        for num, box in enumerate(self.radioButton_X):
            if box.isChecked():
                label=str(self.lineEdit_Name[num].text())
        #getting rid of the eventual units
        if label.find('(')==-1:
            pass
        else:
            label=label[0:label.rfind('(')]
        return label
        
        
    def get_Y_axis_labels(self):  
        """Update which data is used for the Y axis (both left and right)"""
        labels = []
        
        for num, box in enumerate(self.checkBox_Y):
            if box.isChecked():
                label=str(self.lineEdit_Name[num].text())
                label=label[0:label.rfind('(')]
                labels.append(label)
        for num, box in enumerate(self.checkBox_YR):
            if box.isChecked():
                label=str(self.lineEdit_Name[num].text())
                label=label[0:label.rfind('(')]
                if not label in labels:
                    labels.append(label)
        return labels
    
    def get_X_axis_index(self):
        """Update which data is used for the X axis"""
        index=0
        for num, box in enumerate(self.radioButton_X):
            if box.isChecked():
                index=num
        return index
        
        
    def get_Y_axis_index(self):  
        """Update which data is used for the Y axis (both left and right)"""
        index = 0
        for num, box in enumerate(self.checkBox_Y):
            if box.isChecked():
                index=num
        return index
    
    def lineEditHandler(self,mystr):
        print mystr

    def convert_timestamp(self, timestamp):
        dts = map(datetime.datetime.fromtimestamp, timestamp)
        return dates.date2num(dts) # converted        

#    def set_axis_time(self):
#
#        time_data = self.convert_timestamp(self.data_array[:,self.chan_X])
#        xlim = self.convert_timestamp(self.ax.get_xlim())
#        print xlim
#        print time_data
#        # matplotlib date format object
#        hfmt = dates.DateFormatter('%m/%d %H:%M')        
#        
#
#        #plt.xticks(rotation='vertical')
#        #plt.subplots_adjust(bottom=.3)
#        #plt.show()
#        for li in self.ax.lines:
#            if li.get_xdata().size >0:
#                print li.get_data()
#                li.set_xdata(time_data)
#                
#        #self.ax.xaxis.set_major_locator(dates.SecondLocator())
#        #self.ax.xaxis.set_major_formatter(hfmt)
#       
#        print "ready to draw"
#        self.mplwidget.rescale_and_draw()             

    def update_colors(self, color_list):
        for idx, color in enumerate(color_list):
            if idx < len(self.ax.lines):
                self.ax.lines[idx].set_color(color)
                self.axR.lines[idx].set_color(color)                
        self.mplwidget.rescale_and_draw()                 

    def update_labels(self, label_list):
        for idx, label_text in enumerate(label_list):
            if idx == len(self.lineEdit_Name):
                self.add_channel_controls()
                
            self.lineEdit_Name[idx].setText(label_text)             

        
    def update_plot(self, data_array = None): 
#        print "update_plot",data_array
        if not data_array == None:
            self.data_array = data_array
            # if the number of columns is more than the number of control boxes
        try:
            while self.num_channels < self.data_array.shape[1]:
                self.add_channel_controls()
        except:
            pass
        
        if self.data_array.size > 0:
                xdata = self.data_array[:,self.chan_X]         
        else:
            xdata = np.array([0])
                
        for chan_Y, [line_L, line_R] in enumerate(zip (self.ax.lines, self.axR.lines)):
            if self.data_array.size>0:
                if self.checkBox_invert[chan_Y].isChecked():
                    ydata = -self.data_array[:, chan_Y]
                else:
                    ydata = self.data_array[:, chan_Y]                        
                #look which checkbox is checked and plot corresponding data
                if self.checkBox_Y[chan_Y].isChecked() and self.data_array.size>0:
                    line_L.set_data(xdata, ydata)
                    
                else:
                    line_L.set_data([],[])
                    
                #look which checkbox is checked and plot corresponding data    
                if self.checkBox_YR[chan_Y].isChecked() and self.data_array.size>0:
                    line_R.set_data(xdata, ydata)
                else:
                    line_R.set_data([],[])   
            else:
                line_L.set_data([],[])
                line_R.set_data([],[])
#        else:
#            print self.data_array
            line_L.set_linestyle('None')
            line_L.set_marker('o')
        #call a method defined in the module mplZoomwidget.py  
        self.mplwidget.rescale_and_draw() 

        """ put the latest data into the line objects"""
#        first = 0
#        if self.history_length:
#            first = max(0, self.data_array.shape[0] - self.history_length)
# 
#        
#        if self.chan_X == 0:
#            xdata = self.convert_timestamp(self.data_array[first:,self.chan_X])   
#            
#            self.ax.set_xlim(xdata[0], xdata[-1])
#            #self.ax.xaxis.set_major_locator(dates.SecondLocator())
#            hfmt = dates.DateFormatter('%H:%M:%S')   
#            self.ax.xaxis.set_major_formatter(hfmt)
#            labels = self.ax.get_xticklabels()
#            for label in labels:
#                label.set_rotation(90)
#            #self.ax.set_xticklabels(rotation='vertical')
#         
#        else:
#            # avoid the behavior of using offsets on the plot if the scale is small relative to the offset
#            self.ax.xaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
    def remove_fit(self):
        self.ax.lines[-1].set_data([], [])
        self.mplwidget.rescale_and_draw()
        
    def update_fit(self,data_array = None):
#        print "PDW.update_fit",data_array
        if not data_array == None:
            # if the number of columns is more than the number of control boxes
#            print "chan num", self.num_channels
#            print len(self.ax.lines)
            if self.num_channels == len(self.ax.lines):
                line, = self.ax.plot([],[])
#                print line
                
                
            if data_array.size > 0:
#                print "updating"
                xdata = data_array[0]    
                ydata = data_array[1]
                self.ax.lines[-1].set_data(xdata, ydata)
            else:
                self.ax.lines[-1].set_data([],[])
#                print len(self.ax.lines)
#                print self.ax.lines[-1]
#                self.ax.lines[-1].set_color('or')
                                     
            #call a method defined in the module mplZoomwidget.py         
            self.mplwidget.rescale_and_draw() 
            
            
    def print_figure(self, file_name = "unknown"):
        """Sends the current plot to a printer"""
        
        printer = QPrinter()
        
        # Get the printer information from a QPrinter dialog box
        dlg = QPrintDialog(printer)        
        if(dlg.exec_()!= QDialog.Accepted):
             return
             
        p = QPainter(printer)
        
        # dpi*3 because otherwise it looks pixelated (not sure why, bug?)
        dpi = printer.resolution()
        
#        # copy the current figure contents to a standard size figure
#        fig2 = plt.figure(figsize=(8,5), dpi = dpi)
#        
#        ax = fig2.add_subplot(1,1,1)
#        for line in self.ax.lines:
#            if line.get_xdata() != []:
#                ax.plot (line.get_xdata(), line.get_ydata(), label= line.get_label())
#        ax.set_xlim(self.ax.get_xlim())
#        ax.set_ylim(self.ax.get_ylim())
#        ax.set_xlabel(self.ax.get_xlabel())
#        ax.set_ylabel(self.ax.get_ylabel())        
#        
#        self.fig.savefig(
#        # support for printing right axes should go here        
#        
#        # Shink current axis by 20%
#        box = ax.get_position()
#        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        
        # Put a legend to the right of the current axis
        #ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        
        # goal: print the figure with the same aspect ratio as it's shown on the 
        # screen, but fixed width to fill the page nicely
 
        margin_inches = 0.5
        paper_width = 8.5
        
        max_width = paper_width - 2*margin_inches
        max_height = 7
        
        width = self.fig.bbox_inches.width
        height = self.fig.bbox_inches.height
        
        ratio = height/width 
        if ratio > max_height/max_width:
            # scale on width, because otherwise won't fit on page.
            dpi_scale = max_height/height
            height = max_height
            width = ratio/height
        else:
            dpi_scale = max_width/width
            width = max_width
            height = ratio * width
            
        self.fig.savefig("temp.png", dpi=dpi * dpi_scale * 10) 

        
        # half inch margins
        margin_top = 0.5*dpi
        margin_left = 0.5*dpi       
        
        # matplotlib's svg rendering has a bug if the data extends beyond the
        # plot limits. Below is what would be used for temp.svg
        #svg = QtSvg.QSvgRenderer("temp.svg")
        #svg.render(p, QRectF(margin_top,margin_left, 8*dpi, 5*dpi))

        p.drawImage(QRectF(margin_top,margin_left, width*dpi, height*dpi), 
                    QImage("temp.png", format='png'))
        p.drawText(margin_left, 600, "Data recorded to: " + file_name)    
        p.end() 
        
    def is_plot_display_window(self):
        return True

        
# This snippet makes it run as a standalone program
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    form = PlotDisplayWindow()
    form.show()
    app.exec_()
