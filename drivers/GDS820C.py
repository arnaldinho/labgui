# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 18:25:10 2014
GDS820C 150MHz Ocsilloscope
@author: pfduc
"""

#!/usr/bin/env python  
import numpy as np
import time
import random
import Tool  


param={'V':'V'}

class Instrument(Tool.MeasInstr):  
    
    def __init__(self, resource_name, debug=False,**keyw): 
        super(Instrument, self).__init__(resource_name,'GDS820C',debug,baud_rate=19200,term_chars='\n',**keyw)
        
    def __del__(self):
        super(Instrument, self).__del__()      

    def measure(self,channel='V'):
        if self.last_measure.has_key(channel):
            if not self.debug: 
                answer=self.ask(':READ?') #  0 #this is to be defined for record sweep
                answer = float(answer.split(',',1)[0])
                
            else:
                answer=random.random()
            self.last_measure[channel]=answer
        else:
            print "you are trying to measure a non existent channel : " +channel
            print "existing channels :", self.channels
            answer=None
        return answer


        