# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 17:19:35 2013

Copyright (C) 10th april 2015  Benjamin Schmidt & Pierre-Francois Duc
License: see LICENSE.txt file
"""
from PyQt4.QtGui import *
from PyQt4.QtCore import *
try:
    from PyQt4.QtCore import QString
except ImportError:
    QString = str

try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
    
import readconfigfile
try:
    import visa
except:
    print "you will need visa to use our drivers"
    
import readconfigfile
import alarm_toolbox as alarm
import numpy as np

 #Tobe moved into InstrumentHub class as this class should only be GUI
def refresh_device_port_list(): 
    """ Load hardware port list for use in combo boxes """       
    try:
        available_ports = visa.get_instruments_list()
    except:# visa.VisaIOError as e:
        #if e.error_code == -1073807343:
        
        available_ports = ["GPIB0::" +str(i) for i in range(30)]
    return available_ports

class MeasInstr(object):
    #identify the instrument in a unique way        
    ID_name=None
    #store the pyvisa object connecting to the hardware
    connexion=None
    #debug mode trigger
    debug=None
#        print self.debug
    #contains the different channels availiable
    channels=[]
    #store the instrument last measure in the different channels
    last_measure={}
    #contains the units of the different channels
    units={}
    #contains the name of the different channels if the channels do not have explicit names
    channels_names={}
    
    
    #**keyw can be any of the following param "timeout", "term_chars","chunk_size", "lock","delay", "send_end","values_format"
    #example inst=MeasInstr('GPIB0::0','Inst_name',True,timeout=12,term_char='\n')
    #other exemple inst=MeasInstr('GPIB0::0',timeout=12,term_char='\n')
    def __init__(self,resource_name,name='default',debug_value=False,**keyw):
       
        self.ID_name=name
        self.debug=debug_value
        self.resource_name = resource_name
        #load the parameters and their unit for an instrument from the values contained in the global variable of the latter's module
        if name != 'default':
            module_name=__import__(name)
            self.channels=[]
            for chan,u in module_name.param.items():
        #initializes the first measured value to 0 and the channels' names 
                self.channels.append(chan)
                self.units[chan]=u
                self.last_measure[chan]=0
                self.channels_names[chan]=chan

        #establishs a connexion with the instrument
        self.connect(resource_name,**keyw)
#        print self.identify()
               
    def __del__(self):
        pass
        if not self.debug:
            self.close()
        # print "an instrument is dead, it's name was "+self.ID_name

    
    def identify(self,msg=''):
        if not self.debug:
            return msg+self.ask('*IDN?')
        else:
            return msg+self.ID_name 

    def read(self):
        # Reads data available on the port (up to a term. char)
        if not self.debug:
            answer=self.connexion.read()
        else:
            answer=None
        return answer

    def write(self,msg):
        # Writes a message but does not check for a response (non-query command)
        if not self.debug:            
            answer=self.connexion.write(msg)
        else:
            answer=msg
        return answer
            
    def ask(self,msg):
        # Writes a message and reads the reply
        if not self.debug:
            answer=self.connexion.ask(msg)
        else:
            answer=msg
        return answer

    def connect(self,resource_name,**keyw):
        for a in keyw:
            print a
        if not self.debug:    
            self.close();
            try:
                self.connexion=visa.instrument(resource_name,**keyw)
#                print "connected to "+str(resource_name)
            except:
                pass
#                print "unable to connect to "+str(resource_name)
               
        
    def close(self):
        if self.connexion:
            try:
                self.connexion.close()
#                print "disconnect "+self.ID_name
            except:
                pass
#                print "unable to disconnect  "+self.ID_name
        
    def clear(self):
        if self.connexion:
            try:
                self.connexion.clear()
                print "cleared "+self.ID_name
            except:
                print "unable to clear  "+self.ID_name
        
        
    def measure(self,channel):
        #define this method so any instrument has a defined method measure()
        return None
        
    def query_alarm(self):
        #the meaning of the values in bound is to be checked in the file in the function get_settings()
        #one might change that and feed the fname directely to the constructor... or to the query_alarm function     
        bounds=alarm.get_settings(self.ID_name)
        #if the bounds are empty there are no alarm to be set for this instrument
        if len(bounds)==0:
            return [alarm.alarm_handle(0,self.ID_name,"NoAlarmSet")]
        else:
            #store the different alarms output
            alarm_stack=[]        
            #go over each channel and compare the minand max value with the measured value
            for i,chan in enumerate(self.channels):
                #measured value
                value=self.last_measure[chan]
                #upper and lower bonds
                min_val=bounds[2*i];max_val=bounds[2*i+1]
                if min_val==None:min_val=-np.inf
                if max_val==None:max_val=np.inf
                #Channel name
                chan_name=self.channels_names[chan]

                if value>=min_val and value<=max_val:
                    alarm_record=alarm.alarm_handle(0,self.ID_name+"_"+chan_name,chan_name+" "+str(value)+self.units[chan])
                else:
                    if value<min_val:
                        alarm_record=alarm.alarm_handle(1,self.ID_name+"_"+chan_name,chan_name +" is below "+ str(min_val) + " : "+str(value)+self.units[chan]) 
                    if value>max_val:
                        alarm_record=alarm.alarm_handle(1,self.ID_name+"_"+chan_name,chan_name +" is above " + str(max_val) + " : "+str(value)+self.units[chan])
                alarm_stack.append(alarm_record)
            return alarm_stack        
        

class InstrumentHub(QObject):
    def __init__(self,parent=None):
        super(InstrumentHub, self).__init__(parent)
        print "InstrumentHub created"
        
        self.debug=readconfigfile.get_debug_setting()

        # this will be the list of [GPIB address, parameter name] pairs        
        self.port_param_pairs = []

        self.instrument_list={}
        self.parameter_list=[]
        
    def __del__(self):
        self.clean_up()
        print "InstrumentHub deleted"
    
    def connect_hub(self,type_list, dev_list, param_list):
        
        #first close the connections and clear the lists
        self.clean_up()
        none_counter = 0
        
        for instr_name, device_port, param in zip(type_list, dev_list, param_list):
            if device_port in self.instrument_list:
                # Another data channel already used this instrument - make
                # sure it's the same type!!!
                if instr_name != self.instrument_list[device_port].ID_name:
                    print ("Same GPIB device_port specified for different instruments! ")
                    print (device_port + " " + instr_name + " " + self.instrument_list[device_port].ID_name)
                    instr_name = 'NONE'  
            else:
                if instr_name!='' and instr_name!= 'NONE':
                    self.instrument_list[device_port] = self.connect_instrument(instr_name,device_port,self.debug)
            
            if instr_name!='' and instr_name!= 'NONE':
                self.port_param_pairs.append([device_port, param])
            else:
                self.port_param_pairs.append([None, None])
        print self.port_param_pairs

        
        self.emit(SIGNAL("changed_list()"))
        
    def connect_instrument(self,inst_name,device_port,debug=False):
        
        class_inst=__import__(inst_name)
        #device_port should contain the name of the GPIB or the COM port
        if inst_name=="TIME":
            obj=class_inst.Instrument()
        else:
            obj=class_inst.Instrument(device_port,debug)
        return obj
   
    def get_instrument_list(self):
        return self.instrument_list      
   
    def get_parameter_list(self):
        return self.parameter_list    

    def get_port_param_pairs(self):
        return self.port_param_pairs 
    
    def get_instrument_nb(self):
        return len(self.port_param_pairs)
        
    def clean_up(self):
        for key, inst in self.instrument_list.items():
            if key:
                inst.close()    
        self.instrument_list={}
        self.port_param_pairs=[]
        self.instrument_list[None] = None
    
#try to connect to all ports availiable and send *IDN? command
#this is something than can take some time
def whoisthere():
    port_addresses=visa.get_instruments_list()
    
    connexion_list={}
    for port in port_addresses:
        try:
            print port
            device=Tool.MeasInstr(port)
            device.connexion.timeout=0.5
#            print port +" OK"
            try:
                name=device.identify()
                connexion_list[port]=name
            except:
                pass

        except:
            pass
    return connexion_list
#    import types
#
#def str_to_class(field):
#    try:
#        identifier = getattr(sys.modules[__name__], field)
#    except AttributeError:
#        raise NameError("%s doesn't exist." % field)
#    if isinstance(identifier, (types.ClassType, types.TypeType)):
#        return identifier
#    raise TypeError("%s is not a class." % field)
        
        
        
#DEBUGGING/UNDERSTANDING INHERITAGE CODE
#class MI_papa(object):
#    name='default'
#    def __init__(self,rename):
#        self.name=rename;
#        print "a new instrument was created it's name is "+self.name
#    
#    def __del__(self):
#        print " an instrument is dead, it's name was "+self.name
#
#
#class MI_enfant(MI_papa):
#    def __init__(self,rename):
#        super(MI_enfant,self).__init__(rename)
#        print "I am from MI_1"
#
#class A(object):
#    prop1=0
#    def __init__(self):
#        print "world"
#        self.idn()
#        self.prop1=1
#    def idn(self):
#        print "A"
#
#class B(A):
#    def __init__(self):
#        print "hello"
#        self.prop1=2
##        super(B,self).__init__()
#    def idn(self):
#        print "B"