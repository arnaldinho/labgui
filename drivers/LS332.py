# -*- coding: utf-8 -*-
"""
Created on Tue May 29 14:28:29 2012
Lakeshore 332 Driver
@author: Bram
"""

import random
import Tool

param={'A':'K', 'B':'K'}

class Instrument(Tool.MeasInstr):  

    def __init__(self, resource_name, debug=False): 
        super(Instrument, self).__init__(resource_name,'LS332',debug)
        print self.identify()
    
    def __del__(self):
        super(Instrument, self).__del__()
 
    def measure(self,channel):
        if self.last_measure.has_key(channel):
            if not self.debug:
                if channel=='A': 
                    answer=float(self.ask('KRDG?'+channel)) #read in kOhm
                if channel=='B': 
                    answer=float(self.ask('KRDG?'+channel)) #read in kOhm

            else:
                answer=random.random()
            self.last_measure[channel]=answer
        else:
            print "you are trying to measure a non existent channel : " +channel
            print "existing channels :", self.channels
            answer=None
        return answer