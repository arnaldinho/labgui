# README #

This README would normally document whatever steps are necessary to get your application up and running.
But I only set it up for a Pycon talk so I haven't got time to make it nice and consistent

### What is this repository for? ###

This is to communicate with scientific measure instruments and collect data using these instruments, plotting the data, fitting the data and saving it into files. It does the job and it is much simpler than LabView for more complicated experiences.


### How do I get set up? ###

* Summary of set up
You go in the recordsweep folder and look for LabGui.py, then you run that file.
The configuration details are contained in the file named config.txt
You might have to add the other folder to your python path manually, if you know how to automate that process I will be happy to learn that!

* Configuration
You need PyVisa 1.4 (the older version, I haven't moved to the newer one yet)

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* Repo owner pfduc@physics.mcgill.ca or schmidtb@physics.mcgill.ca